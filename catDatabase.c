#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "catDatabase.h"
#define MAX_CATS 10
#define MAX_NAME_LENGTH 30
//five arrays index represents what cat it is

//enum catgender{UNKNOWN_GENDER,MALE,FEMALE} testOne = MALE;
//enum catbreed{UNKNOWN_BREED,MAINE_COON,MANX,SHORTHAIR,PERSIAN,SPHYNX} testTwo = MANX;

enum catgender genders [MAX_CATS];
enum catbreed breeds [MAX_CATS];
bool isFixed [MAX_CATS];//assume cat isnt fixed unless otherwise known
float weights [MAX_CATS];//cant be negative
char names [MAX_CATS][MAX_NAME_LENGTH];

int numberOfCats = 0;
int initializeDatabase(){
   numberOfCats = 0;
   memset(&genders[0],0,sizeof(genders));
   memset(&breeds[0],0,sizeof(breeds));
   memset(&isFixed[0],0,sizeof(isFixed));
   memset(&weights[0],0,sizeof(weights));
   memset(&names[0],0,sizeof(names));

   return 0;
}



