#include <stdio.h>
#include "catDatabase.h"

int printCat(int index){
   if(index<0||index>=numberOfCats){
      printf("animalFarm0: Bad cat [%d]", index);
   }

   printf("cat index=[%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", index, names[index], genders[index], breeds[index], isFixed[index], weights[index]);
   return 0;
}

int printAllCats(){
   for(int i=0;i<numberOfCats;i++){
      printCat(i);
   }
   return 0;
}

int findCat(char name[]){
   for(int i=0;i<(sizeof(names)/sizeof(names[0]));i++){
      if(strcmp(name,names[i])==0){
         return i;
      }
   }
   return -1;
}
