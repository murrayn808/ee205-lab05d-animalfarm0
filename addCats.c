#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "catDatabase.h"
#define MAX_CATS 10
 
//if true contains element that is the same
bool containsElement(char name[],char names[][MAX_NAME_LENGTH]){
   for(int i=0;i<numberOfCats;i++){
      if(strcmp(name,names[i])==0){
         return true;
      }
   }
   return false;
}    

int addCat(char name[],enum catgender gender,enum catbreed breed,bool isCatFixed,float weight){
   if(numberOfCats>=MAX_CATS){
      return -1;
   }
   if(strlen(name)==0){
      return -2;
   }
   if(strlen(name)>30){
      return -3;
   }
   if(containsElement(name, names)){
      return -4;
   }
   if(weight<=0){
      return -5;
   }

   genders[numberOfCats]=gender;
   breeds[numberOfCats]=breed;
   isFixed[numberOfCats]=isCatFixed;
   weights[numberOfCats]=weight;
   strcpy(names[numberOfCats],name);

   int locationOfCat = numberOfCats;
   numberOfCats++;
   return locationOfCat;
}
