#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#pragma once
#define MAX_CATS 10
#define MAX_NAME_LENGTH 30

enum catgender{UNKNOWN_GENDER,MALE,FEMALE};
enum catbreed{UNKNOWN_BREED,MAINE_COON,MANX,SHORTHAIR,PERSIAN,SPHYNX};


//extern catgender testOne;
extern enum catgender genders[MAX_CATS];
//extern catbreed testTwo;
extern enum catbreed breeds[MAX_CATS];
extern bool isFixed[MAX_CATS];
extern float weights[MAX_CATS];
extern char names[MAX_CATS][MAX_NAME_LENGTH];
extern int initializeDatabase();
extern int numberOfCats; 
