#include <stdio.h>
#include "catDatabase.h"
#pragma once

int updateCatName(int index, char newName[]);
int fixCat(int index);
int updateCatWeight(int index, float newWeight);

