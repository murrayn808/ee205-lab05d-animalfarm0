#include <stdio.h>
#include "catDatabase.h"
#include "addCats.h"

int updateCatName(int index, char newName[]){
   if(index<0||index>=numberOfCats){
      return -1;
   }
   if(containsElement(newName,names)){
      printf("Can't update cat name. There is already a cat named %s.\n", newName);
      return -2;
   }
   strcpy(names[index],newName);
   return 0;
}

int fixCat(int index){
   if(index<0||index>=numberOfCats){
      return -1;
   }
   isFixed[index]=true;
   return 0;
}

int updateCatWeight(int index, float newWeight){
   if(index<0||index>=numberOfCats){
      return -1;
   }
   weights[index]=newWeight;
   return 0;
}
